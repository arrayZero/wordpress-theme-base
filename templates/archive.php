<section class="work-container">
	<?php while (have_posts()) : the_post(); ?>
		<?php get_template_part('templates/content', get_post_type().'-archive'); ?>
	<?php endwhile; ?>
</section>
<div class="pagination">
	<?php previous_posts_link( 'Previous Work' ); ?>
	<?php echo get_next_posts_link( 'More Work' ); ?>
</div>