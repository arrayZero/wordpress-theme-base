<?php get_template_part('templates/archive', 'header'); ?>

<section>
	<div class="container">
		<?php while (have_posts()) : the_post(); ?>
			<?php get_template_part('templates/content', get_post_type().'-archive'); ?>
		<?php endwhile; ?>
	</div>

</section>
